package sr.unasat.factory;

import sr.unasat.daoInterfaces.*;

/**
 * Created by jurian360 on 2/20/2016.
 */
public abstract class DAOFactory {


    public static final int MYSQL = 0;
    public static final int DERBY = 1;
    public static final int HIBERNATE = 2;

    public abstract AdvertisementDAO getAdvertisementDAO();
    public abstract AdvertisementTypeDAO getAdvertisementTypeDAO();
    public abstract AdvertisingBroadcastDAO getAdvertisingBroadcastDAO();
    public abstract ClientDAO getClientDAO();
    public abstract RoleDAO getRoleDAO();
    public abstract UserAdtDAO getUserAdtDAO();
    public abstract UserDAO getUserDAO();
    public abstract UserRoleDAO getUserRoleDAO();
    public abstract VideoFileDAO getVideoFileDAO();

    public static DAOFactory getDAOFactory(int database) {
        switch (database) {
            case MYSQL:
                return null;
            case DERBY:
                return null;
            case HIBERNATE:
                return new HibernateDAOFactory();
            default:
                return null;
        }
    }
}
