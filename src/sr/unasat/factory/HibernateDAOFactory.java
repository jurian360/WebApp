package sr.unasat.factory;

import sr.unasat.daoInterfaces.*;

/**
 * Created by jurian360 on 2/20/2016.
 */
public class HibernateDAOFactory extends DAOFactory {
    @Override
    public AdvertisementDAO getAdvertisementDAO() {
        return null;
    }

    @Override
    public AdvertisementTypeDAO getAdvertisementTypeDAO() {
        return null;
    }

    @Override
    public AdvertisingBroadcastDAO getAdvertisingBroadcastDAO() {
        return null;
    }

    @Override
    public ClientDAO getClientDAO() {
        return null;
    }

    @Override
    public RoleDAO getRoleDAO() {
        return null;
    }

    @Override
    public UserAdtDAO getUserAdtDAO() {
        return null;
    }

    @Override
    public UserDAO getUserDAO() {
        return null;
    }

    @Override
    public UserRoleDAO getUserRoleDAO() {
        return null;
    }

    @Override
    public VideoFileDAO getVideoFileDAO() {
        return null;
    }
}
