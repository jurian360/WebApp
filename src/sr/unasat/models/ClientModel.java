package sr.unasat.models;

import java.util.List;

/**
 * Created by jurian360 on 2/16/2016.
 */
public class ClientModel {
    private int clientId;
    private String clientFirstName;
    private String clientLastName;
    private String clientAdres;
    private List<AdvertisementModel> Advertisements;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getClientAdres() {
        return clientAdres;
    }

    public void setClientAdres(String clientAdres) {
        this.clientAdres = clientAdres;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientModel that = (ClientModel) o;

        if (clientId != that.clientId) return false;
        if (clientFirstName != null ? !clientFirstName.equals(that.clientFirstName) : that.clientFirstName != null)
            return false;
        if (clientLastName != null ? !clientLastName.equals(that.clientLastName) : that.clientLastName != null)
            return false;
        if (clientAdres != null ? !clientAdres.equals(that.clientAdres) : that.clientAdres != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = clientId;
        result = 31 * result + (clientFirstName != null ? clientFirstName.hashCode() : 0);
        result = 31 * result + (clientLastName != null ? clientLastName.hashCode() : 0);
        result = 31 * result + (clientAdres != null ? clientAdres.hashCode() : 0);
        return result;
    }

    public List<AdvertisementModel> getAdvertisements() {
        return Advertisements;
    }

    public void setAdvertisements(List<AdvertisementModel> advertisements) {
        Advertisements = advertisements;
    }
}
