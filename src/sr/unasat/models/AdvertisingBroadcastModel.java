package sr.unasat.models;

import java.io.Serializable;

/**
 * Created by jurian360 on 2/16/2016.
 */
public class AdvertisingBroadcastModel {
    private int advertisingBroadcastId;
    private Serializable advertisingBroadcastDate;
    private Serializable advertisingBroadcastTime;
    private String advertisingBroadcastRepeat;

    public int getAdvertisingBroadcastId() {
        return advertisingBroadcastId;
    }

    public void setAdvertisingBroadcastId(int advertisingBroadcastId) {
        this.advertisingBroadcastId = advertisingBroadcastId;
    }

    public Serializable getAdvertisingBroadcastDate() {
        return advertisingBroadcastDate;
    }

    public void setAdvertisingBroadcastDate(Serializable advertisingBroadcastDate) {
        this.advertisingBroadcastDate = advertisingBroadcastDate;
    }

    public Serializable getAdvertisingBroadcastTime() {
        return advertisingBroadcastTime;
    }

    public void setAdvertisingBroadcastTime(Serializable advertisingBroadcastTime) {
        this.advertisingBroadcastTime = advertisingBroadcastTime;
    }

    public String getAdvertisingBroadcastRepeat() {
        return advertisingBroadcastRepeat;
    }

    public void setAdvertisingBroadcastRepeat(String advertisingBroadcastRepeat) {
        this.advertisingBroadcastRepeat = advertisingBroadcastRepeat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdvertisingBroadcastModel that = (AdvertisingBroadcastModel) o;

        if (advertisingBroadcastId != that.advertisingBroadcastId) return false;
        if (advertisingBroadcastDate != null ? !advertisingBroadcastDate.equals(that.advertisingBroadcastDate) : that.advertisingBroadcastDate != null)
            return false;
        if (advertisingBroadcastTime != null ? !advertisingBroadcastTime.equals(that.advertisingBroadcastTime) : that.advertisingBroadcastTime != null)
            return false;
        if (advertisingBroadcastRepeat != null ? !advertisingBroadcastRepeat.equals(that.advertisingBroadcastRepeat) : that.advertisingBroadcastRepeat != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = advertisingBroadcastId;
        result = 31 * result + (advertisingBroadcastDate != null ? advertisingBroadcastDate.hashCode() : 0);
        result = 31 * result + (advertisingBroadcastTime != null ? advertisingBroadcastTime.hashCode() : 0);
        result = 31 * result + (advertisingBroadcastRepeat != null ? advertisingBroadcastRepeat.hashCode() : 0);
        return result;
    }
}
