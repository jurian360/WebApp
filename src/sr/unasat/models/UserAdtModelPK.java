package sr.unasat.models;

import java.io.Serializable;

/**
 * Created by jurian360 on 2/16/2016.
 */
public class UserAdtModelPK implements Serializable {
    private int userId;
    private int adtId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAdtId() {
        return adtId;
    }

    public void setAdtId(int adtId) {
        this.adtId = adtId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAdtModelPK that = (UserAdtModelPK) o;

        if (userId != that.userId) return false;
        if (adtId != that.adtId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + adtId;
        return result;
    }
}
