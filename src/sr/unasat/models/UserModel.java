package sr.unasat.models;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

/**
 * Created by jurian360 on 2/16/2016.
 */
public class UserModel {
    private int userId;
    private String userName;
    private String userPassword;
    private String userEmail;
    private Timestamp userCreatedDate;
    private Timestamp userLastLogin;
    private Set<RoleModel> roles;
    private Set<AdvertisementTypeModel> AdvertisementTypes;
    private List<AdvertisementTypeModel> CreatedAdvertismentTypes;
    private List<AdvertisementModel> CreatedAdvertisments;
    private List<VideoFileModel> UploadedVideos;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Timestamp getUserCreatedDate() {
        return userCreatedDate;
    }

    public void setUserCreatedDate(Timestamp userCreatedDate) {
        this.userCreatedDate = userCreatedDate;
    }

    public Timestamp getUserLastLogin() {
        return userLastLogin;
    }

    public void setUserLastLogin(Timestamp userLastLogin) {
        this.userLastLogin = userLastLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserModel userModel = (UserModel) o;

        if (userId != userModel.userId) return false;
        if (userName != null ? !userName.equals(userModel.userName) : userModel.userName != null) return false;
        if (userPassword != null ? !userPassword.equals(userModel.userPassword) : userModel.userPassword != null)
            return false;
        if (userEmail != null ? !userEmail.equals(userModel.userEmail) : userModel.userEmail != null) return false;
        if (userCreatedDate != null ? !userCreatedDate.equals(userModel.userCreatedDate) : userModel.userCreatedDate != null)
            return false;
        if (userLastLogin != null ? !userLastLogin.equals(userModel.userLastLogin) : userModel.userLastLogin != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (userPassword != null ? userPassword.hashCode() : 0);
        result = 31 * result + (userEmail != null ? userEmail.hashCode() : 0);
        result = 31 * result + (userCreatedDate != null ? userCreatedDate.hashCode() : 0);
        result = 31 * result + (userLastLogin != null ? userLastLogin.hashCode() : 0);
        return result;
    }

    public Set<RoleModel> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleModel> roles) {
        this.roles = roles;
    }

    public Set<AdvertisementTypeModel> getAdvertisementTypes() {
        return AdvertisementTypes;
    }

    public void setAdvertisementTypes(Set<AdvertisementTypeModel> advertisementTypes) {
        AdvertisementTypes = advertisementTypes;
    }

    public List<AdvertisementTypeModel> getCreatedAdvertismentTypes() {
        return CreatedAdvertismentTypes;
    }

    public void setCreatedAdvertismentTypes(List<AdvertisementTypeModel> createdAdvertismentTypes) {
        CreatedAdvertismentTypes = createdAdvertismentTypes;
    }

    public List<AdvertisementModel> getCreatedAdvertisments() {
        return CreatedAdvertisments;
    }

    public void setCreatedAdvertisments(List<AdvertisementModel> createdAdvertisments) {
        CreatedAdvertisments = createdAdvertisments;
    }

    public List<VideoFileModel> getUploadedVideos() {
        return UploadedVideos;
    }

    public void setUploadedVideos(List<VideoFileModel> uploadedVideos) {
        UploadedVideos = uploadedVideos;
    }
}
