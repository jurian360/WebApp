package sr.unasat.models;

import java.util.Set;

/**
 * Created by jurian360 on 2/16/2016.
 */
public class AdvertisementTypeModel {
    private int adtId;
    private String adtName;
    private String adtPricePerSecond;
    private Set<UserModel> Users;

    public int getAdtId() {
        return adtId;
    }

    public void setAdtId(int adtId) {
        this.adtId = adtId;
    }

    public String getAdtName() {
        return adtName;
    }

    public void setAdtName(String adtName) {
        this.adtName = adtName;
    }

    public String getAdtPricePerSecond() {
        return adtPricePerSecond;
    }

    public void setAdtPricePerSecond(String adtPricePerSecond) {
        this.adtPricePerSecond = adtPricePerSecond;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdvertisementTypeModel that = (AdvertisementTypeModel) o;

        if (adtId != that.adtId) return false;
        if (adtName != null ? !adtName.equals(that.adtName) : that.adtName != null) return false;
        if (adtPricePerSecond != null ? !adtPricePerSecond.equals(that.adtPricePerSecond) : that.adtPricePerSecond != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = adtId;
        result = 31 * result + (adtName != null ? adtName.hashCode() : 0);
        result = 31 * result + (adtPricePerSecond != null ? adtPricePerSecond.hashCode() : 0);
        return result;
    }

    public Set<UserModel> getUsers() {
        return Users;
    }

    public void setUsers(Set<UserModel> users) {
        Users = users;
    }
}
