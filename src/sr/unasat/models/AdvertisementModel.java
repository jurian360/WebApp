package sr.unasat.models;

import java.util.List;

/**
 * Created by jurian360 on 2/16/2016.
 */
public class AdvertisementModel {
    private int advertisementId;
    private String advertisementName;
    private List<VideoFileModel> VideoFiles;
    private AdvertisingBroadcastModel AdvertisingBroadcast;

    public int getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(int advertisementId) {
        this.advertisementId = advertisementId;
    }

    public String getAdvertisementName() {
        return advertisementName;
    }

    public void setAdvertisementName(String advertisementName) {
        this.advertisementName = advertisementName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdvertisementModel that = (AdvertisementModel) o;

        if (advertisementId != that.advertisementId) return false;
        if (advertisementName != null ? !advertisementName.equals(that.advertisementName) : that.advertisementName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = advertisementId;
        result = 31 * result + (advertisementName != null ? advertisementName.hashCode() : 0);
        return result;
    }

    public List<VideoFileModel> getVideoFiles() {
        return VideoFiles;
    }

    public void setVideoFiles(List<VideoFileModel> videoFiles) {
        VideoFiles = videoFiles;
    }

    public AdvertisingBroadcastModel getAdvertisingBroadcast() {
        return AdvertisingBroadcast;
    }

    public void setAdvertisingBroadcast(AdvertisingBroadcastModel advertisingBroadcast) {
        AdvertisingBroadcast = advertisingBroadcast;
    }
}
