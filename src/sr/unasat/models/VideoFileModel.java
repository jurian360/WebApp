package sr.unasat.models;

/**
 * Created by jurian360 on 2/16/2016.
 */
public class VideoFileModel {
    private int VideoFileID;
    private String VideoFileName;
    private String VideoFileExtension;
    private int VideoFileLength;
    private String VideoFilePath;

    public int getVideoFileID() {
        return VideoFileID;
    }

    public void setVideoFileID(int videoFileID) {
        VideoFileID = videoFileID;
    }

    public String getVideoFileName() {
        return VideoFileName;
    }

    public void setVideoFileName(String videoFileName) {
        VideoFileName = videoFileName;
    }

    public String getVideoFileExtension() {
        return VideoFileExtension;
    }

    public void setVideoFileExtension(String videoFileExtension) {
        VideoFileExtension = videoFileExtension;
    }

    public int getVideoFileLength() {
        return VideoFileLength;
    }

    public void setVideoFileLength(int videoFileLength) {
        VideoFileLength = videoFileLength;
    }

    public String getVideoFilePath() {
        return VideoFilePath;
    }

    public void setVideoFilePath(String videoFilePath) {
        VideoFilePath = videoFilePath;
    }
}
