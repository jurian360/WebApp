package sr.unasat.models;

import java.sql.Timestamp;

/**
 * Created by jurian360 on 2/16/2016.
 */
public class UserAdtModel {
    private int userId;
    private int adtId;
    private Timestamp updatedDateTime;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAdtId() {
        return adtId;
    }

    public void setAdtId(int adtId) {
        this.adtId = adtId;
    }

    public Timestamp getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Timestamp updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAdtModel that = (UserAdtModel) o;

        if (userId != that.userId) return false;
        if (adtId != that.adtId) return false;
        if (updatedDateTime != null ? !updatedDateTime.equals(that.updatedDateTime) : that.updatedDateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + adtId;
        result = 31 * result + (updatedDateTime != null ? updatedDateTime.hashCode() : 0);
        return result;
    }
}
